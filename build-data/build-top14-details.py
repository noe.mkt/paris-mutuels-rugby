import requests
import os
import json

data_path = os.path.join("..", "data", "top14")

# For each game capture the json stats file en utilisant le numéro dans
# head -> link rel="shortlink" et en l'insérant dans https://www.lnr.fr/fdmi?nid=710402
base_url = "https://www.lnr.fr/fdmi?nid="
for f in os.listdir(data_path):
    if "Saison" in f:
        season_name = ".".join(f.split('.')[:-1])
        print(f"Processing {season_name}")
        season_data = json.load(open(os.path.join(data_path, f)))
        for matchday in season_data:
            for game_id in season_data[matchday]:
                target = os.path.join(
                    data_path, "game_details", "lnr_details", str(game_id) + ".json"
                )
                if not os.path.exists(target):
                    r = requests.get(base_url + str(game_id))
                    if r.status_code == 200:
                        d = r.json()
                        json.dump(
                            d,
                            open(target, "w")
                        )
