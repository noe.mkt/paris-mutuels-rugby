from flask import abort
import requests
import logging
import os
import time

from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import selenium.webdriver.support.ui as ui
from scrapy.selector import Selector


DIR_PATH = os.path.dirname(os.path.realpath(__file__))


class Scraper:

    def __init__(self, config, logger=None):
        options = Options()
        options.add_argument("--headless")
        fp = webdriver.FirefoxProfile(config["mozilla_profile"])
        self._driver = webdriver.Firefox(firefox_options=options, firefox_profile=fp)
        self._scrapers = {
            "the-economist": self._scrape_the_economist,
            "les-echos": self._scrape_les_echos
        }
        if not logger:
            formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
            fh = logging.FileHandler(os.path.join(DIR_PATH, "scraper.log"))
            fh.setLevel(logging.DEBUG)
            fh.setFormatter(formatter)
            logger = logging.getLogger("scraper")
            logger.addHandler(fh)
            logger.setLevel(logging.DEBUG)
            self._logger = logger
        else:
            self._logger = logger
        self._logger.info("Scraper initialized.")

    def __exit__(self):
        self._logger.info("Scraper destroyed.")
        self._driver.close()

    def supported_scrapers(self):
        return self._scrapers.keys()

    def scrape(self, source, url):
        self._logger.debug("Scraping article \"{}\" from source {}".format(url, source))
        if source not in self._scrapers.keys():
            self._logger.debug("Source {} not supported.".format(source))
            abort(500)
        else:
            self._logger.debug("Launching scraper for url {} with source {}.".format(url, source))
            return self._scrapers[source](url)

    def _scrape_les_echos(self, url, failures=0):
        try:
            article = {
                "source": "les-echos"
            }
            texts = []
            self._driver.delete_all_cookies()
            self._driver.get(url)
            text_sels = ui.WebDriverWait(self._driver, 5).until(
                lambda browser: browser.find_elements_by_xpath("//div[contains(@class, 'paywall')]/*")
            )
            self._logger.debug("Retrieving core text for article {}.".format(url))
            for text_sel in text_sels:
                if text_sel.tag_name in ["h3", "h4"]:
                    texts.append({
                        "tag": "h4",
                        "text": text_sel.get_attribute('textContent')
                    })
                elif text_sel.tag_name == "p":
                    texts.append({
                        "tag": "p",
                        "text": text_sel.get_attribute('textContent')
                    })
            article["texts"] = texts
            article["title"] = ui.WebDriverWait(self._driver, 5).until(
                lambda browser: browser
                                 .find_elements_by_xpath("//header[contains(@class, 'main-header-article')]/h1")
            )
            try:
                article["title"] = article["title"][0].get_attribute('textContent')
            except IndexError:
                article["title"] = ""
            return article
        except Exception as e:
            self._logger.debug("An error occurred while scraping url {} with scraper {}: \"{}\"".format(
                url, "les-echos", str(e)
            ))
            if failures >= 3:
                self._logger.debug("3 errors occurred while scraping url {} with scraper {}: \"{}\"".format(
                    url, "les-echos", str(e)
                ))
                abort(500)
            else:
                time.sleep(2)
                self._scrape_les_echos(url, failures + 1)

    def _scrape_the_economist(self, url, failures=0):
        try:
            article = {}
            self._driver.get(url)
            texts = []
            text_sels = ui.WebDriverWait(self._driver, 5).until(
                lambda browser: browser.find_elements_by_xpath(
                    "//div[contains(@class, 'blog-post__text')]/child::node()"
                )
            )
            self._logger.debug("Retrieving core text for article {}.".format(url))
            for text_sel in text_sels:
                if text_sel.tag_name == "figure":  # filter out ads and newsletter
                    texts.append({
                        "tag": "img",
                        "text": text_sel.find_element_by_xpath('.//img').get_attribute("src")
                    })
                elif text_sel.tag_name == "p":
                    if "xhead" == text_sel.get_attribute("class"):
                        texts.append({
                            "tag": "h4",
                            "text": text_sel.get_attribute('textContent')
                        })
                    else:
                        texts.append({
                            "tag": "p",
                            "text": text_sel.get_attribute('textContent')
                        })
            article["texts"] = texts
            title_sel = '//h1/span[contains(@class, "flytitle-and-title__title")]'
            article["title"] = self._driver.find_element_by_xpath(title_sel).get_attribute("textContent")
            article["source"] = "the-economist"
            return article
        except Exception as e:
            self._logger.debug("An error occurred while scraping url {} with scraper {}: \"{}\"".format(
                url, "the-economist", str(e)
            ))
            if failures >= 3:
                self._logger.debug("3 errors occurred while scraping url {} with scraper {}: \"{}\"".format(
                    url, "the-economist", str(e)
                ))
                abort(500)
            else:
                time.sleep(2)
                self._scrape_the_economist(url, failures + 1)

    @staticmethod
    def eco_scrape_categ(url, page=1):
        if page != 1:
            url += "?page=" + str(page)
        r = requests.get(url)
        main_sel = Selector(text=r.content)
        articles_sel = main_sel.xpath('//article')
        articles = []
        for article_sel in articles_sel:
            article = {
                "title": article_sel.xpath(
                    './/span[contains(@class, "flytitle-and-title__title")]/text()'
                ).extract_first(),
                "description": article_sel.xpath(
                    './/div[contains(@class, "teaser__text")]/text()'
                ).extract_first(),
                "datetime": article_sel.xpath(
                    './/div[contains(@class, "teaser__datetime")]/time/@datetime'
                ).extract_first(),
                "image": article_sel.xpath(
                    './/div[contains(@class, "component-image")]/img/@src'
                ).extract_first(),
                "url": article_sel.xpath(
                    './a[contains(@class, "teaser__link")]/@href'
                ).extract_first()
            }
            article["formatted_url"] = "/the-economist/article?url=" + "https://www.economist.com" + article["url"]
            article["formatted_date"] = article["datetime"][:10] + " " + article["datetime"][-9:-4]
            articles.append(article)
        try:
            max_page = int(
                main_sel.xpath('//div[contains(@class, "pagination")]/ul/li')[-1].xpath('./*')[-1]
                        .xpath('./text()').extract_first()
            )
        except IndexError:
            max_page = 1
        return max_page, articles
