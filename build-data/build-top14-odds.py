from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import selenium.webdriver.support.ui as ui
import re
import time
import pandas as pd
import os


start_url = "https://www.oddsportal.com/rugby-union/france/top-14/results/"
data_path = os.path.join("..", "data", "top14")


# Initialization
options = Options()
# options.add_argument("--headless")
fp = webdriver.FirefoxProfile("/home/valentin/.mozilla/firefox/5g19k4xn.default")
fp = None  # pas besoin de profil si pas besoin d'adblocker
driver = webdriver.Firefox(firefox_options=options, firefox_profile=fp)
driver.get(start_url)


seasons = []
seasons_urls = []
li_sels = ui.WebDriverWait(driver, 5).until(
    lambda browser: browser.find_elements_by_xpath("//li//a")
)
for sel in li_sels:
    text = sel.get_attribute("textContent").strip()
    if re.match(r"\d{4}\/\d{4}", text):
        seasons.append(text)
        season_url = sel.get_attribute("href")
        seasons_urls.append(season_url)
        print(f"Found season {text} at {season_url}.")


# Scrape each season games
for season, season_url in zip(seasons, seasons_urls):
    time.sleep(1)
    print(f"Processing season {season}.")
    games = pd.DataFrame(columns=["raw_datetime", "home_team", "away_team", "home_score", "away_score",
                                  "home_win_main_odd", "home_loss_main_odd", "draw_main_odd", "n_bookies"])
    driver.get(season_url)
    count = 0
    while True:
        cur_date = None
        row_sels = ui.WebDriverWait(driver, 5).until(
            lambda browser: browser.find_elements_by_xpath(
                "//table[@id='tournamentTable']//tr"
            )
        )
        for row_sel in row_sels[1:]:  # The first one is the row with the tournament's name
            if "dummyrow" not in row_sel.get_attribute("class"):
                text = row_sel.get_attribute("textContent")
                match_date = re.match(r"^(\d\d? \w+ \d{4})", text)
                if match_date:
                    cur_date = match_date.group(1)
                else:
                    cell_sels = row_sel.find_elements_by_xpath("./td")
                    raw_datetime = f"{cur_date} {cell_sels[0].get_attribute('textContent')}"
                    games.loc[count, "raw_datetime"] = raw_datetime
                    t = cell_sels[1].get_attribute("textContent")
                    t = t.replace("Mont-de-Marsan", "Mont de Marsan")
                    t = t.replace("CS Bourgoin-Jallieu", "CS Bourgoin Jallieu")
                    home_team, away_team = t.split('-')
                    games.loc[count, "home_team"] = home_team.strip()
                    games.loc[count, "away_team"] = away_team.strip()
                    home_score, away_score = cell_sels[2].get_attribute("textContent").split(':')
                    games.loc[count, "home_score"] = home_score.strip()
                    games.loc[count, "away_score"] = away_score.strip()
                    try:
                        games.loc[count, "home_win_main_odd"] = float(
                            cell_sels[3].get_attribute("textContent")
                        )
                    except ValueError:
                        games.loc[count, "home_win_main_odd"] = None
                    try:
                        games.loc[count, "home_loss_main_odd"] = float(
                            cell_sels[5].get_attribute("textContent")
                        )
                    except ValueError:
                        games.loc[count, "home_loss_main_odd"] = None
                    try:
                        games.loc[count, "draw_main_odd"] = float(cell_sels[4].get_attribute("textContent"))
                    except ValueError:
                        games.loc[count, "draw_main_odd"] = None
                    try:
                        games.loc[count, "n_bookies"] = int(cell_sels[6].get_attribute("textContent"))
                    except ValueError:
                        games.loc[count, "n_bookies"] = None
                    count += 1
        # Navigation
        nav_sels = ui.WebDriverWait(driver, 5).until(
            lambda browser: browser.find_elements_by_xpath("//div[@id='pagination']/a")
        )
        next_page_url = nav_sels[-2].get_attribute("href")
        if next_page_url == driver.current_url:
            break
        else:
            driver.get(next_page_url)
            time.sleep(3)
    # Persistence
    real_index = list(games.index)[::-1]
    games.loc[:, "season_order"] = real_index
    games.set_index("season_order", inplace=True)
    target = os.path.join(data_path, f"odds_{season.replace('/', '_')}.csv")
    games.to_csv(target)

driver.close()
