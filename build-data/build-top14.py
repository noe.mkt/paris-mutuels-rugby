from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import TimeoutException
import selenium.webdriver.support.ui as ui
import json
import os
import re
import time


# Function to test for modals and close them
def close_modals(d):
    modal_close_l = d.find_elements_by_xpath("//div[@class='mc-closeModal']")
    if len(modal_close_l) > 0:
        modal_close_l[0].click()
        time.sleep(0.5)


# Helper function to scrape all the data for a game available on the website page and not
# on the game details
def get_available_game_info(driver_tmp):
    d = dict()
    d["home_team_url"] = ui.WebDriverWait(driver_tmp, 5).until(
        lambda browser: browser.find_element_by_xpath("//div[contains(@class, 'team-a')]/a")
    ).get_attribute("href")
    d["home_team_name"] = ui.WebDriverWait(driver_tmp, 5).until(
        lambda browser: browser.find_element_by_xpath("//div[contains(@class, 'team-a')]/a//h2")
    ).get_attribute("textContent")
    d["home_team_score"] = ui.WebDriverWait(driver_tmp, 5).until(
        lambda browser: browser.find_element_by_xpath("//span[contains(@class, 'score-a')]")
    ).get_attribute("textContent")
    d["away_team_url"] = ui.WebDriverWait(driver_tmp, 5).until(
        lambda browser: browser.find_element_by_xpath("//div[contains(@class, 'team-b')]/a")
    ).get_attribute("href")
    d["away_team_name"] = ui.WebDriverWait(driver_tmp, 5).until(
        lambda browser: browser.find_element_by_xpath("//div[contains(@class, 'team-b')]/a//h2")
    ).get_attribute("textContent")
    d["away_team_score"] = ui.WebDriverWait(driver_tmp, 5).until(
        lambda browser: browser.find_element_by_xpath("//span[contains(@class, 'score-b')]")
    ).get_attribute("textContent")
    sels = ui.WebDriverWait(driver_tmp, 5).until(
        lambda browser: browser.find_elements_by_xpath("//ul[contains(@class, 'infos-list')]/li/span")
    )
    d["raw_gamedate"] = None
    d["raw_gametime"] = None
    for sel in sels:
        s = sel.get_attribute("textContent")
        if re.match(r"\w+ \d\d? \w+ \d{4}", s):
            d["raw_gamedate"] = s
        elif re.match(r"\d{2}h\d{2}", s):
            d["raw_gametime"] = s
    return d


# Initialization
options = Options()
options.add_argument("--headless")
fp = webdriver.FirefoxProfile("/home/valentin/.mozilla/firefox/5g19k4xn.default")
fp = None  # pas besoin de profil si pas besoin d'adblocker
driver = webdriver.Firefox(firefox_options=options, firefox_profile=fp)
start_url = "https://www.lnr.fr/rugby-top-14/calendrier-resultats-rugby-top-14"
driver.get(start_url)

# Initialize data
orders = {}
game_urls_dict = {}
data_path = os.path.join("..", "data", "top14")

# Select season tab
tabs_sel = ui.WebDriverWait(driver, 5).until(
    lambda browser: browser.find_elements_by_xpath(
        "//dl[contains(@class, 'tabs')]/dd"
    )
)
close_modals(driver)
tabs_sel[0].click()

# Browse seasons
print(f"Finding seasons' urls")
season_sels = ui.WebDriverWait(driver, 5).until(
    lambda browser: browser.find_elements_by_xpath(
        "//div[@id='panel-filter-season']//li//a"
    )
)
season_urls = [sel.get_attribute("href") for sel in season_sels]
season_names = [sel.get_attribute("title").strip() for sel in season_sels]
orders["seasons"] = season_names

# Browse each season
for season_url, season_name in zip(season_urls[:-1][::-1], season_names[:-1][::-1]):
    # Processing season
    if os.path.exists(os.path.join(data_path, f"{season_name}.json")):
        print(f"{season_name} is already processed.")
    else:
        print(f"Processing season {season_name}")
        season_data = {}
        driver.get(season_url)
        matchday_sels = ui.WebDriverWait(driver, 5).until(
            lambda browser: browser.find_elements_by_xpath(
                "//div[@id='panel-filter-day']//li//a"
            )
        )
        matchday_names = [sel.get_attribute("textContent").strip() for sel in matchday_sels]
        matchday_urls = [sel.get_attribute("href") for sel in matchday_sels]
        orders[season_name] = matchday_names
        for matchday, url in zip(matchday_names, matchday_urls):
            # Reinitialize driver to avoid memory overflow
            driver.close()
            driver = webdriver.Firefox(firefox_options=options, firefox_profile=fp)
            print(f"Processing matchday {matchday} ({season_name})")
            season_data[matchday] = []
            driver.get(url)
            try:
                game_sels = ui.WebDriverWait(driver, 5).until(
                    lambda browser: browser.find_elements_by_xpath(
                        "//div[@class='day-results-table']//tr[contains(@class, 'detail-after-match')]//a[1]"
                    )
                )
            except TimeoutException:
                with open("errors.log", "a") as error_f:
                    error_f.write(
                        f"Error retrieving {matchday}, {season_name}.\n"
                    )
                game_sels = []
            game_sels = game_sels[::3]
            game_urls = [sel.get_attribute("href") for sel in game_sels]
            game_urls_dict[season_name] = game_urls
            for game_url in game_urls:
                driver.get(game_url)
                game_id = int(ui.WebDriverWait(driver, 5).until(
                    lambda browser: browser.find_element_by_xpath(
                        "//head/link[@rel='shortlink']"
                    )
                ).get_attribute('href').split('/')[-1])
                season_data[matchday].append(game_id)
                game_scraped_data = get_available_game_info(driver)
                game_data_target = os.path.join(
                    data_path, "game_details", "scraped_details", str(game_id) + ".json"
                )
                with open(game_data_target, "w") as game_data_f:
                    json.dump(game_scraped_data, game_data_f, indent=4)
                print(game_id)
        # Persist season data
        json.dump(season_data, open(os.path.join(data_path, f"{season_name}.json"), "w"))

# Persist data
skip_meta_persistence = True
if not skip_meta_persistence:
    json.dump(orders, open(os.path.join(data_path, "data_ordering.json"), "w"))
    json.dump(game_urls_dict, open(os.path.join(data_path, "game_urls.json"), "w"))

# Close driver
driver.close()
