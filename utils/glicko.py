import numpy as np
from collections import defaultdict


class Glicko:

    def __init__(self, q=None, c=None, plt=None):
        self.q = q
        self.c = c
        self.plt = plt

    def _weekly_evolve_rd(self, rd, n_weeks):
        n_weeks = int(n_weeks)
        if n_weeks <= 0:
            return min(350, rd)
        else:
            return self._weekly_evolve_rd(
                np.sqrt(np.power(rd, 2) + np.power(self.c, 2)), n_weeks - 1
            )

    def _aux_g(self, rd):
        return np.power(1 + 3 * np.power(self.q * rd / np.pi, 2), -1 / 2)

    def _aux_esp(self, r, ro, rdo):
        return 1 / (1 + np.power(10, - self._aux_g(rdo) * (r - ro) / 400))

    def _aux_dsquare(self, r, ro, rdo):
        esp_tmp = self._aux_esp(r, ro, rdo)
        return 1 / (np.power(self.q * self._aux_g(rdo), 2) * esp_tmp * (1 - esp_tmp))

    def _aux_glicko(self, r, rd, ro, rdo, s):  # s is the outcome
        denom = 1 / (rd ** 2) + 1 / self._aux_dsquare(r, ro, rdo)
        new_r = r + self.q * self._aux_g(rdo) * (s - self._aux_esp(r, ro, rdo)) / denom
        new_rd = np.power(denom, - 1 / 2)
        return new_r, new_rd

    def glicko(self, games, init_rating=None, init_rds=None):
        """
        Adds columns with glicko ratings and rating deviations in the given dataframe. Handles only
        one season.
        :param games: The dataframe containing the game information (retrieved via
        gamesloader.Top14Loader.get_consolidated_games).
        :param init_rating: Initial values for the ratings.
        :param init_rds: Initial values for the rating deviations.
        :return: The updated dataframe, a dictionary of the team ratings
        and a dictionary of the team rating deviations.
        """
        if self.q is None or self.c is None:
            raise ValueError("q or c attribute has not been instantiated.")
        games = games.sort_values(["season", "season_order"])
        teams = set(games["home_team"]) | set(games["away_team"])
        team_ratings = {}
        team_rds = {}
        if init_rating is None:
            for team in teams:
                team_ratings[team] = 1500
                team_rds[team] = 350
        else:
            # Use the previons ratings (np array of dim n_teams, 2) to calculate the new ratings for the
            # new teams (use the same rating as previously for teams staying in the game and
            # using the mean
            # of the rating of the exiting team for the entering teams) with an expanded rating deviation
            # (even more expanded for the new teams)
            for team in teams:
                if team in init_rating:
                    team_ratings[team] = init_rating[team]
                    team_rds[team] = self._weekly_evolve_rd(init_rds[team], 12)
                else:
                    team_ratings[team] = 1500
                    team_rds[team] = 350
        # Initialize teams_last_game
        teams_last_game = {}
        for team in teams:
            team_filter = (games["home_team"] == team) | (games["away_team"] == team)
            teams_last_game[team] = list(games[team_filter]["datetime"])[0]
        # Process
        for games_index, row in games.iterrows():
            home_team = row["home_team"]
            away_team = row["away_team"]
            # Compute s (outcome)
            s = 0
            if row["home_score"] > row["away_score"]:
                s = 1
            elif row["home_score"] == row["away_score"]:
                s = 1 / 2
            # retrieve current values
            r = team_ratings[home_team]
            rd = team_rds[home_team]
            ro = team_ratings[away_team]
            rdo = team_rds[away_team]
            # Update rating deviation
            home_game_dt = (row["datetime"] - teams_last_game[home_team]).days / 7
            away_game_dt = (row["datetime"] - teams_last_game[away_team]).days / 7
            prev_rd = self._weekly_evolve_rd(rd, home_game_dt)
            prev_rdo = self._weekly_evolve_rd(rdo, away_game_dt)
            # Compute
            new_r_home, new_rd_home = self._aux_glicko(r, prev_rd, ro, prev_rdo, s)
            new_r_away, new_rd_away = self._aux_glicko(ro, prev_rdo, r, prev_rd, 1 - s)
            # Update
            # ----- Store result
            games.loc[games_index, "home_r"] = new_r_home
            games.loc[games_index, "home_rd"] = new_rd_home
            games.loc[games_index, "away_r"] = new_r_away
            games.loc[games_index, "away_rd"] = new_rd_away
            games.loc[games_index, "prev_home_r"] = r
            games.loc[games_index, "prev_away_r"] = ro
            games.loc[games_index, "prev_home_rd"] = rd
            games.loc[games_index, "prev_away_rd"] = rdo
            # ----- Update function helper variables
            team_ratings[home_team] = new_r_home
            team_ratings[away_team] = new_r_away
            team_rds[home_team] = new_rd_home
            team_rds[away_team] = new_rd_away
            teams_last_game[home_team] = row["datetime"]
            teams_last_game[away_team] = row["datetime"]
        return games, team_ratings, team_rds

    def plot_glicko(self, df, show=False, legend=True, teams=None):
        """
        Helper function to plot the glicko rating evolution for the different teams.
        :param df: The dataframe containing the games information with the ratings.
        :param show: Boolean, indicates whether plt.show() should be called. Defaults to False.
        :param legend: Boolean, indicates whether the legend should be shown. Defaults to True.
        :param teams: A list of the teams for which the glicko rating should be plotted.
        :return: None
        """
        if teams is None:
            teams = set(df["home_team"]) | set(df["away_team"])
        else:
            teams_set = set(df["home_team"]) | set(df["away_team"])
            for team in teams:
                if team not in teams_set:
                    raise ValueError(f"The team {team} is not in the set of teams.")
        if self.plt is None:
            raise ValueError("The plt attribute of the Glicko instance has not been initialized.")
        teams_ratings = defaultdict(list)
        teams_games_dt = defaultdict(list)
        for _, row in df.iterrows():
            home_team = row["home_team"]
            away_team = row["away_team"]
            teams_ratings[home_team].append(row["home_r"])
            teams_ratings[away_team].append(row["away_r"])
            teams_games_dt[home_team].append(row["datetime"])
            teams_games_dt[away_team].append(row["datetime"])
        for team in teams:
            self.plt.plot(teams_games_dt[team], teams_ratings[team], "--o", label=team)
        if legend:
            self.plt.legend()
        if show:
            self.plt.show()
