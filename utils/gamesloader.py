import os
import re
import json
import numpy as np
import pandas as pd
from datetime import datetime

from utils.glicko import Glicko


script_dir = os.path.dirname(os.path.realpath(__file__))
months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet",
          "Août",
          "Septembre", "Octobre", "Novembre", "Décembre"]


class Top14Loader:
    """
    Loader class to handle top 14 games data.
    Can retrieve
    """
    data_path = os.path.join(script_dir, "..", "data", "top14")

    seasons = []
    for f in os.listdir(os.path.join(data_path)):
        if "Saison" in f:
            seasons.append(".".join(f.split('.')[:-1]))

    season_matchdays = {}
    for season in seasons:
        season_matchdays[season] = []
        season_data = json.load(open(os.path.join(data_path, season + ".json")))
        for matchday in season_data:
            season_matchdays[season].append(matchday)

    team_correspondance = {
        'Agen': 'Agen',
        'Albi': 'Albi',
        'Clermont': 'Clermont',
        'Lyon': 'Lyon',
        'Racing 92': 'Racing 92',
        'La Rochelle': 'La Rochelle',
        'Montauban': 'Montauban',
        'Biarritz': 'Biarritz Olympique',
        'Brive': 'CA Brive',
        'Mont-de-Marsan': 'Mont de Marsan',
        'Montpellier': 'Montpellier Herault RC',
        'Toulon': 'RC Toulonnais',
        'Castres': 'Castres Olympique',
        'Paris': 'Stade Francais Paris',
        'Oyonnax': 'Union Sportive Oyonnax',
        'Bayonne': 'Aviron Bayonnais',
        'Bourgoin': 'CS Bourgoin Jallieu',
        'Bordeaux Begles': 'Bordeaux-Bègles',
        'Bordeaux-Bègles': 'Bordeaux Begles',
        'Grenoble': 'Grenoble FC',
        'Perpignan': 'USA Perpignan',
        'Pau': 'Section Paloise',
        'Toulouse': 'Stade Toulousain'
    }

    @staticmethod
    def _parsedatetime(s_date, s_time):
        if not re.match(r"\w+ \d\d? \w+ \d{4}", s_date.strip()):
            return None
        s_date = s_date.strip()
        day = int(s_date.split(' ')[1])
        month = months.index(s_date.split(' ')[2]) + 1
        year = int(s_date.split(' ')[-1])

        if re.match(r"\d{2}h\d{2}", s_time.strip()):
            s_time = s_time.strip()
            hour = int(s_time.split('h')[0])
            minutes = int(s_time.split('h')[1])

            return datetime(year, month, day, hour, minutes)
        else:
            return datetime(year, month, day)

    @staticmethod
    def _parse_raw_datetime(s):
        months_index = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        match_dt = re.match(r"(\d{2}) (\w{3}) (\d{4}) (\d{2}):(\d{2})", s)
        day, month, year, hour, minute = match_dt.groups()
        return datetime(int(year), months_index.index(month) + 1, int(day), int(hour), int(minute))

    @staticmethod
    def get_games(stats=False):
        """
        Retrieve the LNR games stored.
        :param stats: Whether stats detailed by the LNR should be included.
        :return: A dataframe containing the games.
        """
        if stats:
            raise NotImplementedError("Stats retrieval not yet implemented.")

        df = pd.DataFrame(columns=["season", "matchday", "overall_index", "season_index", "home_team",
                                   "away_team", "home_score", "away_score", "datetime"])

        count = 0
        for season in Top14Loader.seasons:
            season_count = 0
            for matchday in Top14Loader.season_matchdays[season]:
                season_data = json.load(
                    open(os.path.join(Top14Loader.data_path, season + ".json"), "r")
                )
                for game_id in season_data[matchday]:
                    season_match = re.match(r"^\w+ (\d{4})", season)
                    df.loc[game_id, "season"] = int(season_match.group(1))
                    df.loc[game_id, "matchday"] = matchday
                    df.loc[game_id, "overall_index"] = count
                    df.loc[game_id, "season_index"] = season_count
                    game_data = json.load(
                        open(os.path.join(Top14Loader.data_path, "game_details",
                                          "scraped_details", f"{game_id}.json"), "r")
                    )
                    df.loc[game_id, "home_team"] = game_data["home_team_name"]
                    df.loc[game_id, "home_score"] = game_data["home_team_score"]
                    df.loc[game_id, "away_team"] = game_data["away_team_name"]
                    df.loc[game_id, "away_score"] = game_data["away_team_score"]
                    if game_data["raw_gamedate"] is not None and game_data["raw_gametime"] is not None:
                        df.loc[game_id, "datetime"] = Top14Loader._parsedatetime(
                            game_data["raw_gamedate"], game_data["raw_gametime"]
                        )

                    # Update counted for indexing
                    count += 1
                    season_count += 1
        numeric_cols = ["season", "overall_index", "season_index", "home_score", "away_score"]
        for col in numeric_cols:
            df.loc[:, col] = pd.to_numeric(df[col])
        df = df[df["season"] >= 2009]
        df.loc[:, "home_team"] = df["home_team"].apply(lambda s: Top14Loader.team_correspondance[s])
        df.loc[:, "away_team"] = df["away_team"].apply(lambda s: Top14Loader.team_correspondance[s])
        return df

    @staticmethod
    def get_odds(season=None):
        """
        Retrieve the odds scraped from oddsportal.
        :param season: What season should be retrieved. If None, all seasons are retrieved. Defaults to None.
        :return: A dataframe containing the data scraped from oddsportal.
        """
        if season:
            season_name = f"{season}_{season + 1}"
            target = os.path.join(Top14Loader.data_path, f"odds_{season_name}.csv")
            df = pd.read_csv(target)
            df.loc[:, "season"] = int(season)
        else:
            season_name = f"2008_2009"
            target = os.path.join(Top14Loader.data_path, f"odds_{season_name}.csv")
            df = pd.read_csv(target)
            df.loc[:, "season"] = 2008
            for season in range(2009, 2018):
                season_name = f"{season}_{season + 1}"
                target = os.path.join(Top14Loader.data_path, f"odds_{season_name}.csv")
                df2 = pd.read_csv(target)
                df2.loc[:, "season"] = season
                df = pd.concat([df, df2])
        # Process away score to delete bonuses
        df.loc[:, "away_score"] = df["away_score"].apply(lambda s: int(str(s).strip().replace("\xa0OT", "")))
        return df[df["season"] >= 2009]

    @staticmethod
    def get_consolidated_games(season=None, compute_glicko=False):
        """
        Retrieves both the data from oddsportal and the LNR. Consolidates the games to match the data
        between both source.
        :param season: What season should be retrieved. If None, all seasons are retrieved. Defaults to None.
        :param compute_glicko: Boolean to indicate if the glicko rating should be computed. Defaults to False.
        :return: A dataframe containing the data scraped from both sources. A LNR id column can be used to
        retrieve more statistics about the games.
        """
        if season and season not in range(2009, 2018):
            raise ValueError('Season must be between 2009 and 2017 included.')
        games_df = Top14Loader.get_games()
        if season:
            games_df = games_df[games_df["season"] == season]
        odds_df = Top14Loader.get_odds(season=season)
        for season in set(odds_df["season"]):
            season_odds = odds_df[odds_df["season"] == season]
            season_lnr = games_df[games_df["season"] == season]
            gb_odds = season_odds.groupby(["home_team", "away_team"])["raw_datetime"].count()
            gb_lnr = season_lnr.groupby(["home_team", "away_team"])["matchday"].count()
            for home_team, away_team in zip(season_odds["home_team"], season_odds["away_team"]):
                b = (home_team, away_team) in gb_lnr
                b = b and gb_odds[home_team, away_team] == gb_lnr[home_team, away_team]
                if b:
                    for i in range(gb_odds[home_team, away_team]):
                        filter1 = (season_odds["home_team"] == home_team) & \
                                  (season_odds["away_team"] == away_team)
                        odds_index = season_odds[filter1].index[i]
                        filter2 = (season_lnr["home_team"] == home_team) & \
                                  (season_lnr["away_team"] == away_team)
                        odds_df.loc[odds_index, "lnr_id"] = int(season_lnr[filter2].index[i])
                else:
                    for i in range(gb_odds[home_team, away_team]):
                        filter1 = (season_odds["home_team"] == home_team) & (season_odds["away_team"] == away_team)
                        odds_index = season_odds[filter1].index[i]
                        odds_df.loc[odds_index, "lnr_id"] = np.nan
                    pass
        odds_df.loc[:, "datetime"] = odds_df["raw_datetime"].apply(Top14Loader._parse_raw_datetime)
        odds_df.drop("raw_datetime", axis=1, inplace=True)
        odds_df.reset_index(inplace=True)
        odds_df = odds_df.sort_values(by=["season", "season_order"])
        if compute_glicko:
            glicko = Glicko(q=np.log(10) / 2000, c=60)
            if season is not None:  # Glicko function handles only one season
                odds_df, _, _ = glicko.glicko(odds_df)
            else:
                for season in range(2009, 2018):
                    season_df = odds_df[odds_df["season"] == season]
                    odds_df.loc[odds_df["season"] == season] = glicko.glicko(season_df)
                raise NotImplementedError("It's implemented but we don't have values consistent with" 
                                          " season by season glicko computing.")
        odds_df.drop("index", axis=1, inplace=True)
        return odds_df
